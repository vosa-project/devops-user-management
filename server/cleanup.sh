#!/bin/bash
# Script for cleaning all the history, logs and etc in VirtualTA labs
# Source this script from rc.local

# Author - Johannes Tammekänd
#
# Date - 09.03.2016
# Version - 0.0.1

# If VM is not a template, then run the cleaning

TEMPLATE="VirtualBox"
DMIDECODEVERSION="$(dmidecode -s bios-version | cut -d'-' -f1,2)"

if [ "$DMIDECODEVERSION" != "$TEMPLATE" ]; then

	# Cleanup logs, history and delete any unneccesary files
	
	echo | tee /var/log/*.log
	echo | tee /var/log/*.log.*
	echo | tee /var/log/boot.log
	echo | tee /var/log/auth*
	echo | tee /var/log/apache2/*
	echo | tee /var/log/apt/*
	echo | tee /var/log/dmesg*
	echo | tee /var/log/debug*
	echo | tee /var/log/messages*
	echo | tee /var/log/kern*
	echo | tee /var/log/syslog*
	echo | tee /var/log/mysql/*
	echo | tee /var/log/mysql*
	echo | tee /var/log/nginx/*
	echo | tee /var/log/udev*
	echo | tee /var/log/dpkg*
	echo | tee /root/.viminfo
	echo | tee /root/.vim/*
	echo | tee /root/.bash_history
	echo | tee /root/.mysql_history
	echo | tee /root/.ssh/known_hosts
	echo | tee /root/.vnc/*.log
	echo | tee /root/.vnc/passwd
	echo | tee /home/student/.viminfo
	echo | tee /home/student/.bash_history
	echo | tee /home/student/.mysql_history
	echo | tee /home/student/.ssh/known_hosts
	echo | tee /var/log/xferlog*
	echo | tee /var/log/alternatives*
	echo | tee /var/log/samba/*
	echo | tee /var/log/tomcat5/*
	echo | tee /var/log/tomcat6/*
	echo | tee /var/log/tomcat7/*
	echo | tee /var/log/landscape/*
	echo | tee /var/log/ntpstats/*
	echo | tee /var/log/upstart/*
	echo | tee /var/log/ConsoleKit/*
	echo | tee /var/log/cups/*
	echo | tee /var/log/fsck/*
	echo | tee /var/log/installer/*
	echo | tee /var/log/lightdm/*
	echo | tee /var/log/upstart/*
	echo | tee /var/log/ntpd.log
	echo | tee /var/log/clamd.log
	echo | tee /var/log/userlog.info
	rm -f /var/log/*.gz
	rm -f /var/log/apache2/*.gz
	rm -f /home/student/Downloads/*
	
	# Clean firefox history
	
	bleachbit --clean firefox.*
	
	# Cleanup rc.local
	
	sed -ie '/Run clean/,+1d' /etc/rc.local
	
	# Delete the script once it's run
	
	rm $0 && exit 0

fi 
