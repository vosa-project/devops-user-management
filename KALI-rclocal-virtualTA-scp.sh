#!/bin/bash

#Author: Katrin Loodus
#Last modify date: 31.03.2016

#Description: This script is used for copying back rc.local and virtualTA.sh files to Kali desktop after they have been modified and fetched from the repository. 

#Important! Run this script AFTER you have synced the directory with git. 

#seccopies updated rc.local and virtualTA.sh scripts to template desktop
find /root/linuxcli-usermanagement -mtime 0 -type f \( -name "rc.local" -o -name "*virtualTA.sh" \) -exec scp {} root@192.168.6.1:/root \;

#seccopies updated rc.local and cleanup.sh scripts to template server
find /root/linuxcli-usermanagement/server -mtime 0 -type f \( -name "rc.local" -o -name "cleanup.sh" \) -exec scp {} root@192.168.6.2:/var/tmp \;

