#!/bin/bash
# Script for checking DEVOPS - user management lab objectives
# Objective name - Add user to a new group

# Author - Katrin Loodus
#
# Date - 03.04.2016
# Version - 0.0.1

LC_ALL=C

# START
# DEVOPSMODGRP

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/tmp/modgroup.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=192.168.6.2

    # Time to sleep between running the check again
    Sleep=5

    # devopsmodgrp.sh specific variables 

    # Objective uname in VirtualTA
    Uname=devopsmodgrp


}

# User interaction: Add to a new group on the command line

DEVOPSMODGRP () {

    while true 
    do 

	   # Check if the user is put in the correct group
        ssh root@$IP_to_SSH getent group team |grep anna

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nUsers have been added successfully! Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Users have not been added successfully! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

DEVOPSMODGRP

exit 0

