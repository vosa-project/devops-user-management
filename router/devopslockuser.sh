#!/bin/bash
# Script for checking DEVOPS - user management lab objectives
# Objective name - Locking the user

# Author - Katrin Loodus
#
# Date - 01.04.2016
# Version - 0.0.1

LC_ALL=C

# START
# DEVOPSLOCKUSER

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/tmp/lockuser.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=192.168.6.2

    # Time to sleep between running the check again
    Sleep=5

    # devopslockuser.sh specific variables 

    # Objective uname in VirtualTA
    Uname=devopslockuser


}

# User interaction: Locks an existing user on the command line

DEVOPSLOCKUSER () {

    while true 
    do 

	   # Check if user has locked the user anna
        ssh root@$IP_to_SSH passwd -S anna |grep L  

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nUser has been locked! Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "User has NOT been locked! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

DEVOPSLOCKUSER

exit 0

