#!/bin/bash
# Script that creates virtualTA desktop icon

if test -f "/home/student/.config/autostart/virtualta.desktop" 

then

touch /tmp/virtualtafileexists

rm "/home/student/.config/autostart/virtualta.desktop" && touch /tmp/virtualtafiledeleted

fi

if [ $(dmidecode -s system-serial-number) != 'System Serial' ]
then
cat > /home/student/.config/autostart/virtualta.desktop <<EOF
[Desktop Entry]
Type=Application
Exec=chromium-browser --app=https://portal.rangeforce.com:8433/lab/$(dmidecode -s system-serial-number) --incognito
Hidden=false
X-MATE-Autostart-enabled=true
Name[en_US]=Start from here
Name=Chromium
Comment[en_US]=
Comment=
Icon=/usr/share/icons/LoginIcons/apps/48/view-refresh.svg
Comment[en_US.UTF-8]=Dashboard
EOF
chown student.student /home/student/.config/autostart/virtualta.desktop
chmod 700 /home/student/.config/autostart/virtualta.desktop
fi

# Wait for the network to come up

counter=10

while [ $counter -gt 0  ]; do

ping -c1 portal.rangeforce.com && touch /tmp/pingsuccess && exit 0 

sleep 1

# Log to syslog if ping fails

touch /tmp/pingfailed 

$((counter--))

done

exit 0

