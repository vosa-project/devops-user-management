#!/bin/bash

export LAB_USERNAME=$(dmidecode -s bios-release-date)

dmidecode -s bios-version |grep '\-devops1-' >/dev/null 2>&1
if [ $? -eq 0 ] 
then 
	LAB_ID=CDHcnkwqjAFzjEEYA
else
	LAB_ID=CDHcnkwqjAFzjEEYA
fi

cat > lab.ini <<EOC
[LAB]
ta_key = ac0b3963fec3a51767ffb4a85936480d
virtualta_hostname = https://portal.rangeforce.com:8433
lab_id = $LAB_ID
EOC

